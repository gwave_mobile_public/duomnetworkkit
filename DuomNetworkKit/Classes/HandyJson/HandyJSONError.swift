//
//  NetworkAPI+RX.swift
//  Duom
//
//  Created by kuroky on 2022/8/17.
//

import Foundation

public enum HandyJSONMapError: Error, LocalizedError {
    case mapError(String)

    public var errorDescription: String? {
        if case let .mapError(json) = self {
            #if DEBUG
                return "Failed to map to HandyJSON object. \(json)"
            #else
                return nil
            #endif
        }
        return nil
    }
}
