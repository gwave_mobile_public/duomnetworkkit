//
//  NetworkAPI+RX.swift
//  Duom
//
//  Created by kuroky on 2022/8/17.
//
import Foundation
import HandyJSON

public final class HandyDataModel<T>: HandyJSON {
    public var code: String?
    public var msgKey: String?
    public var obj: T?
    public var success: Bool?

    public required init() { }
}

extension HandyDataModel {
    public func isSuccess() -> Bool {
        guard let code = code else {
            return false
        }
        return Int.init(code)! == 0000
    }
}
