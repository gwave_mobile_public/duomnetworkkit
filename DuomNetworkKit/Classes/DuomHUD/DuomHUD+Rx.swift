//
//  DuomHUD+RX.swift
//  DuomBase
//
//  Created by kuroky on 2022/10/21.
//

import Foundation
import RxSwift
import MBProgressHUD
import Moya
import DuomBase

@discardableResult
private func showBusinessErrorTip(error: Error,
                                from: UIViewController? = UIApplication.topViewController()) -> Bool {
    guard let type = error.networkError?.type else {
        return false
    }

    switch type {
    case .loginRequired:
        #if DEBUG
        Logger.log(message: "current need to login", level: .error)
        #endif
    default:
        return false
    }
    return true
}

private func getDicFromData(_ data: Data) -> [String: Any]? {
    if let result = (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)) as? [String: Any] {
        return result
    }
    return nil
}

extension PrimitiveSequence where PrimitiveSequence.Trait == RxSwift.SingleTrait {
    public func showHUD(_ mask: HUDMaskStyle = .hide) -> PrimitiveSequence<Trait, Element> {
        var hud: MBProgressHUD?
        return observe(on: MainScheduler.instance)
            .do(onSuccess: { _ in
                hud?.hide(animated: true)
            }, onError: { error in
                hud?.hide(animated: true)
                if !error.localizedDescription.isEmpty {
                    DuomHUD.showError(error.localizedDescription)
                }
            }, onSubscribe: {
                hud = DuomHUD.show(nil, maskStyle: mask)
            }, onDispose: {
                hud?.hide(animated: true)
            }).subscribe(on: ConcurrentMainScheduler.instance)
    }
    
    public func showErrorHud() -> PrimitiveSequence<Trait, Element> {
        return observe(on: MainScheduler.instance)
            .do(onError: { error in
                DuomHUD.dismiss()
                if !error.localizedDescription.isEmpty {
                    DuomHUD.showError(error.localizedDescription)
                }
            })
    }
    
    public func showErrorTipOnWindow(from: UIViewController? = UIApplication.topViewController()) -> PrimitiveSequence<Trait, Element> {
        return observe(on: MainScheduler.instance)
            .do(onError: { error in
                DuomHUD.dismiss()
                if !showBusinessErrorTip(error: error, from: from) {
                    DuomHUD.showError(error.localizedDescription)
                }
            })
    }
}

extension Completable {
   public func showHUD(_ maskStyle: HUDMaskStyle = .hide) -> PrimitiveSequence<Trait, Element> {
        var hud: MBProgressHUD?
        return observe(on: MainScheduler.instance)
            .do(onError: { error in
                hud?.hide(animated: true)
                if !error.localizedDescription.isEmpty {
                    DuomHUD.showError(error.localizedDescription)
                }
            }, onCompleted: {
                hud?.hide(animated: true)
            }, onSubscribe: {
                hud = DuomHUD.show(nil, maskStyle: maskStyle)
            }, onDispose: {
                hud?.hide(animated: true)
            }).subscribe(on: ConcurrentMainScheduler.instance)
    }
    
    public func showErrorHUD() -> PrimitiveSequence<Trait, Element> {
        return observe(on: MainScheduler.instance)
            .do(onError: { error in
                DuomHUD.dismiss()
                if !error.localizedDescription.isEmpty {
                    DuomHUD.showError(error.localizedDescription)
                }
            })
    }
}


extension ObservableType {
    func showHUD(_ maskStyle: HUDMaskStyle = .hide) -> Observable<Element> {
        var hud: MBProgressHUD?
        return observe(on: MainScheduler.instance)
            .do(onNext: { _ in
                hud?.hide(animated: true)
            }, onError: { error in
                hud?.hide(animated: true)
                if !error.localizedDescription.isEmpty {
                    DuomHUD.showError(error.localizedDescription)
                }
            }, onSubscribe: {
                hud = DuomHUD.show(nil, maskStyle: maskStyle)
            }, onDispose: {
                hud?.hide(animated: true)
            }).subscribe(on: ConcurrentMainScheduler.instance)
    }

    func showErrorHUD() -> Observable<Element> {
        return observe(on: MainScheduler.instance)
            .do(onError: { error in
                DuomHUD.dismiss()
                if !error.localizedDescription.isEmpty {
                    DuomHUD.showError(error.localizedDescription)
                }
            })
    }

    func showErrorOrTipWindow(from: UIViewController = UIApplication.topViewController()!,
                              actionPrepareBlock: ((UIViewController) -> Void)? = nil) -> Observable<Element> {
        return observe(on: MainScheduler.instance)
            .do(onError: { error in
                DuomHUD.dismiss()
                if !showBusinessErrorTip(error: error, from: from) {
                    DuomHUD.showError(error.localizedDescription)
                }
            })
    }
}
