//
//  DuomHUD.swift
//  Differentiator
//
//  Created by kuroky on 2022/10/21.
//

import Foundation
import UIKit
import MBProgressHUD
import Lottie
import DuomBase


public enum HUDMaskStyle {
    case mask
    case hide
}

private var HUDKeepAliveKey: Void?

private extension MBProgressHUD {
    var isKeepAlive: Bool {
        get {
            return (objc_getAssociatedObject(self, &HUDKeepAliveKey) as? Bool) ?? false
        }
        set {
            objc_setAssociatedObject(self, &HUDKeepAliveKey, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
}

public class DuomHUD {
    private static let loadingAnimationView = AnimationView(name: "loading_data").then {
        $0.contentMode = .scaleAspectFit
        $0.loopMode = .loop
    }
    
    @discardableResult
    public static func showSuccess(_ text: String? = nil) -> MBProgressHUD {
        return showMessage(text)
    }

    @discardableResult
    public static func showError(_ text: String? = nil) -> MBProgressHUD {
        return showMessage(text)
    }

    @discardableResult
    public static func showInfo(_ text: String? = nil) -> MBProgressHUD {
        return showMessage(text)
    }
    
    @discardableResult
    public static func showMessage(_ text: String? = nil, autoDismissDelay: Double = 3) -> MBProgressHUD {
        let hud = showNewHUD(UIView())
        hud.margin = 16
        hud.mode = .text
        hud.label.numberOfLines = 0
        hud.label.textColor = .white
        hud.detailsLabel.textColor = .white
        hud.detailsLabel.numberOfLines = 0
        hud.label.text = text
        if autoDismissDelay > 0 {
            hud.hide(animated: true, afterDelay: autoDismissDelay)
        } else {
            hud.isKeepAlive = true
        }
        return hud
    }
    
    @discardableResult
    public static func showImage(_ image: UIImage, text: String? = nil, autoDismissDelay: Double = 3) -> MBProgressHUD {
        let imageView = UIImageView(image: image)
        let hud = showNewHUD(imageView, insets: UIEdgeInsets(top: 0, left: 0, bottom: 6, right: 0))
        hud.label.text = text
        if autoDismissDelay > 0 {
            hud.hide(animated: true, afterDelay: autoDismissDelay)
        } else {
            hud.isKeepAlive = true
        }
        
        return hud
    }
    
    @discardableResult
    public static func show(_ maskStyle: HUDMaskStyle = .hide) -> MBProgressHUD {
        return show(nil, maskStyle: maskStyle)
    }

    @discardableResult
    public static func show(_ text: String? = nil, maskStyle: HUDMaskStyle = .hide) -> MBProgressHUD {
        let hudId = (text?.hashValue ?? 0) ^ (maskStyle == .hide ? 100 : 101)

        let hud = showNewHUD(loadingAnimationView)
        hud.tag = hudId
        hud.isUserInteractionEnabled = maskStyle == .mask
        hud.label.text = text
        hud.isKeepAlive = true

        if text.isNilOrEmpty {
            hud.bezelView.color = UIColor.clear
            hud.bezelView.style = .solidColor

            delay(1) { [weak loadingAnimationView, weak hud] in
                if hud?.value(forKey: "finished") as? Bool == false {
                    loadingAnimationView?.play()
                }
            }
        } else {
            loadingAnimationView.play()
        }

        return hud
    }
    
    public static func dismiss(_ animated: Bool = true) {
        visibleHUDs().forEach {
            if $0.isKeepAlive {
                $0.removeFromSuperViewOnHide = true
                $0.hide(animated: animated)
            }
        }
    }
    
    private static func visibleHUDs() -> [MBProgressHUD] {
        return UIApplication.shared.windows.flatMap { $0.subviews.compactMap { $0 as? MBProgressHUD } }
    }
}


public extension DuomHUD {
    private static func showNewHUD(_ customView: UIView, insets: UIEdgeInsets = .zero) -> MBProgressHUD {
        let contentView = UIView()
        contentView.addSubview(customView)
        customView.snp.makeConstraints { make in
            make.edges.equalTo(insets)
        }

        let hud = MBProgressHUD.showAdded(to: topWindow, animated: true)
        hud.bezelView.subviews.forEach {
            if let view = $0 as? UIVisualEffectView {
                view.removeFromSuperview()
            }
        }
        hud.isUserInteractionEnabled = false
        hud.isSquare = false
        hud.bezelView.layer.cornerRadius = 8
        hud.bezelView.color = UIColor.black.withAlphaComponent(0.33)
        hud.bezelView.blurEffectStyle = .dark
        hud.customView = contentView
        hud.mode = .customView
        hud.label.numberOfLines = 0
        hud.label.textColor = .white
        hud.detailsLabel.textColor = .white
        hud.detailsLabel.numberOfLines = 0

        hud.bezelView.bringSubviewToFront(hud.label)
        hud.bezelView.bringSubviewToFront(hud.detailsLabel)
        hud.bezelView.bringSubviewToFront(hud.button)

        return hud
    }

    private static var topWindow: UIWindow {
        let window = UIApplication.shared.windows.last(where: {
            $0.screen == UIScreen.main &&
                !$0.isHidden && $0.alpha > 0 &&
                $0.windowLevel == UIWindow.Level.normal
        })
        return window ?? (UIApplication.shared.keyWindow ?? UIWindow())
    }
}

