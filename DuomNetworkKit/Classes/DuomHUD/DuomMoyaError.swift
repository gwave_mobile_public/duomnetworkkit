//
//  DuomMoyaError.swift
//  DuomNetworkKit
//
//  Created by kuroky on 2022/10/21.
//

import Foundation
import Moya

public enum DuomMoyaErrorType: String {
    case unknown = "0"
    case loginRequired = "1000" // login required

    var description: String? {
        switch self {
        case .loginRequired:
            return "current user is been loginRequired"
        default:
            return nil
        }
    }
}

public struct DuomMoyaError: Error, LocalizedError {
    var code: String
    var message: String?
    var type: DuomMoyaErrorType

    public init(code: String, message: String?) {
        self.code = code
        self.message = message
        type = DuomMoyaErrorType(rawValue: code) ?? .unknown
    }

    public var errorDescription: String? {
        return localizedDescription
    }

    public var localizedDescription: String {
        return message ?? "unkonw error: \(code)"
//        return (type.description ?? message) ?? "unknow error: \(code)"
    }
}

extension MoyaError {
    public var localizedDescription: String {
        let desc = errorDescription ?? ""
        return desc.isEmpty ? "unkonw error" : desc
    }
}

extension Swift.Error {
    public var networkError: DuomMoyaError? {
        if let err = self as? MoyaError,
            case let MoyaError.underlying(networkError, _) = err {
            return networkError as? DuomMoyaError
        }
        
        if let err = self as? DuomMoyaError {
            return err
        }
        
        return nil
    }
}
