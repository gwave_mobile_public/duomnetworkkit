//
//  Typealias.swift
//  Duom
//
//  Created by kuroky on 2022/8/17.
//

import Foundation
import Alamofire
import Moya
import HandyJSON

public typealias APIHost = String
public typealias APIPath = String
public typealias APINumber = Int
public typealias APIMethod = Moya.Method
public typealias APIParameters = Alamofire.Parameters
public typealias APIStubBehavior = Moya.StubBehavior

public typealias JSONCodableEnum = HandyJSONEnum
public protocol JSONCodable: HandyJSON {}


