//
//  NetworkAPI.swift
//  Alamofire
//
//  Created by kuroky on 2022/9/1.
//

import Foundation
import Moya
import Alamofire

public protocol NetworkAPI: Moya.TargetType {
    var parameters: [String: Any] { get }
    // url拼接参数
    var urlParameters: [String: Any] { get }
    var path: String { get }
}

extension NetworkAPI {
    public var headers: [String : String]? {
        if NetworkConfig.baseHeaders.isEmpty {
            return ["Content-type": "application/json"]
        }
        return NetworkConfig.baseHeaders
    }

    public var baseURL: URL {
        return URL(string: NetworkConfig.baseURL)!
    }

    public var method: Moya.Method {
        return .post
    }

    public var sampleData: Data {
        return Data()
    }

    public var parameters: [String: Any] {
        return [:]
    }
    
    public var urlParameters: [String: Any] {
        return [:]
    }
    
    public var task: Moya.Task {
        switch method {
        case .get:
            return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
        case .post:
            if !urlParameters.isEmpty {
                return .requestCompositeParameters(bodyParameters: parameters, bodyEncoding: JSONEncoding.default, urlParameters: urlParameters)
            }
            return .requestParameters(parameters: parameters, encoding: JSONEncoding.default)
        case .put:
            return .requestParameters(parameters: parameters, encoding: JSONEncoding.default)
        default:
            return .requestParameters(parameters: parameters, encoding: JSONEncoding.default)
        }
    }
}

//extension TargetType {
//    public static func provider() -> MoyaProvider<Self> {
//        var session: Moya.Session
//        let configuration = URLSessionConfiguration.af.default
//        configuration.headers = Alamofire.HTTPHeaders.default
//        configuration.timeoutIntervalForRequest = NetworkConfig.timeoutIntervalForRequest
//        configuration.timeoutIntervalForResource = NetworkConfig.timeoutIntervalForResource
//        session = Moya.Session(configuration: configuration, startRequestsImmediately: false)
//        
//        let plugins = NetworkConfig.plugins
//
//         print("--------->  \(Self.self)")
//        // 自定义后台运行级别并行队列
//        let queue = DispatchQueue(label: "com.duom.queue", qos: .background, attributes: [.concurrent])
//        return MoyaProvider<Self>(callbackQueue: queue, session: session, plugins: plugins)
////        return MoyaProvider<Self>(session: session, plugins: plugins)
////        return MoyaProvider<Self>(plugins: plugins)
//    }
//}
