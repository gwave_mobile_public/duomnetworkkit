//
//  PluginSubType.swift
//  DuomNetworkKit
//
//  Created by kuroky on 2022/9/2.
//

import Foundation
import Moya

// 继承PluginType 方便扩展
public protocol PluginSubType: Moya.PluginType {
    var pluginName: String { get }
}
