//
//  NetworkDebuggingPlugin.swift
//  RxNetworks
//
//  Created by Condy on 2021/12/12.
//

import Foundation
import Moya

/// 网络打印，DEBUG模式内置插件
/// Network printing, DEBUG mode built in plugin.
public final class NetworkDebuggingPlugin {
    
    /// Enable print request information.
    public var openDebugRequest: Bool = true
    /// Turn on printing the response result.
    public var openDebugResponse: Bool = true
    
    public init() { }
}

extension NetworkDebuggingPlugin: PluginSubType {
    
    public var pluginName: String {
        return "Debugging"
    }
    
    public func willSend(_ request: RequestType, target: TargetType) {
        #if DEBUG
        printRequest(target, plugins: NetworkConfig.plugins)
        #endif
    }
    
    public func didReceive(_ result: Result<Response, MoyaError>, target: TargetType) {
        #if DEBUG
        ansysisResult(result)
        #endif
    }
}

extension NetworkDebuggingPlugin {
    private func printRequest(_ target: TargetType, plugins: [PluginSubType]) {
        guard openDebugRequest else { return }
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSSSSSZ"
        formatter.locale = Locale.current
        let date = formatter.string(from: Date())
        var parameters: APIParameters? = nil
        if case .requestParameters(let parame, _) = target.task {
            parameters = parame
        }
        if let param = parameters, param.isEmpty == false {
            print("""
                  ------- 🎈 Request 🎈 -------
                  Time: \(date)
                  Method: \(target.method.rawValue)
                  Host: \(target.baseURL.absoluteString)
                  Path: \(target.path)
                  Parameters: \(param)
                  HeaderParameters: \(target.headers ?? [:])
                  Plugins: \(pluginString(plugins))
                  LinkURL: \(requestFullLink(with: target))
                  
                  """)
        } else {
            print("""
                  ------- 🎈 Request 🎈 -------
                  Time: \(date)
                  Method: \(target.method.rawValue)
                  Host: \(target.baseURL.absoluteString)
                  Path: \(target.path)
                  HeaderParameters: \(target.headers ?? [:])
                  Plugins: \(pluginString(plugins))
                  LinkURL: \(requestFullLink(with: target))
                  
                  """)
        }
    }
    
    private func pluginString(_ plugins: [PluginSubType]) -> String {
        return plugins.reduce("") { $0 + $1.pluginName + " " }
    }
    
    private func requestFullLink(with target: TargetType) -> String {
        var parameters: APIParameters? = nil
        if case .requestParameters(let parame, _) = target.task {
            parameters = parame
        }
        guard let parameters = parameters, !parameters.isEmpty else {
            return target.baseURL.absoluteString + target.path
        }
        let sortedParameters = parameters.sorted(by: { $0.key > $1.key })
        var paramString = "?"
        for index in sortedParameters.indices {
            paramString.append("\(sortedParameters[index].key)=\(sortedParameters[index].value)")
            if index != sortedParameters.count - 1 { paramString.append("&") }
        }
        return target.baseURL.absoluteString + target.path + "\(paramString)"
    }
}

extension NetworkDebuggingPlugin {
    private func ansysisResult(_ result: Result<Moya.Response, MoyaError>) {
        switch result {
        case let .success(response):
            do {
                let response = try response.filterSuccessfulStatusCodes()
                let json = try response.mapJSON()
                printResponse(json, true)
            } catch MoyaError.jsonMapping(let response) {
                let error = MoyaError.jsonMapping(response)
                printResponse(error.localizedDescription, false)
            } catch MoyaError.statusCode(let response) {
                let error = MoyaError.statusCode(response)
                printResponse(error.localizedDescription, false)
            } catch {
                printResponse(error.localizedDescription, false)
            }
        case let .failure(error):
            printResponse(error.localizedDescription, false)
        }
    }
    
    private func printResponse(_ json: Any, _ success: Bool) {
        guard openDebugResponse else { return }
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSSSSSZ"
        formatter.locale = Locale.current
        let date = formatter.string(from: Date())
        print("""
              ------- 🎈 Response 🎈 -------
              Time: \(date)
              Result: \(success ? "Successed." : "Failed.")
              Response: \(json)
              
              """)
    }
}
