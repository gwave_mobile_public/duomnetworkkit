//
//  NetworkConfiguration.swift
//  Duom
//
//  Created by kuroky on 2022/8/17.
//

import Foundation
import Moya

public let devAPIHost = "https://apidev.duom.link"

public let betaAPIHost = "https://apibeta.duom.link"

public let alphaAPIHost = "https://apialpha.duom.link"

public let prodAPIHost = "https://api.duom.link"

/// 网络配置信息，只需要在程序开启的时刻配置一次
public struct NetworkConfig {
    public static var timeoutIntervalForRequest: Double = 20
    public static var timeoutIntervalForResource: Double = 120
    public static var plugins: [PluginSubType] = [NetworkDebuggingPlugin(), NetworkAnalysisPlugins()]
    
    public private(set) static var baseURL: APIHost = devAPIHost
    public private(set) static var baseMethod: APIMethod = APIMethod.post
    public static var baseHeaders: [String: String] = ["Content-type": "application/json; charset=UTF-8",
                                                       "saas_id": "duom",
                                                       "Accept": "application/json; charset=UTF-8"
                                                      ]

    public static func setupDefault(header: [String: String] = [:],
                                    method: APIMethod = APIMethod.post,
                                    plugins: [PluginSubType] = [NetworkDebuggingPlugin(), NetworkAnalysisPlugins()]) {
        self.baseMethod = method
        self.baseHeaders += header
        self.plugins = plugins
    }
    
    public static func setupDefaultHost(host: String) {
        switch host {
        case "dev":
            self.baseURL = devAPIHost
        case "beta":
            self.baseURL = betaAPIHost
        case "alpha":
            self.baseURL = alphaAPIHost
        case "prod":
            self.baseURL = prodAPIHost
        default:
            self.baseURL = betaAPIHost
            break
        }
    }
}

