//
//  NetworkAnalysisPlugins.swift
//  DuomNetworkKit
//
//  Created by kuroky on 2022/10/21.
//

import Foundation
import Moya

public final class NetworkAnalysisPlugins: PluginSubType {
    public var pluginName: String {
        return "Analysis"
    }
    
    public init() { }
    
    public func process(_ result: Result<Response, MoyaError>, target: TargetType) -> Result<Response, MoyaError> {
        switch result {
        case let .success(response):
            if let map = getDicFromResponseData(response.data),
               response.statusCode >= 200,
               response.statusCode < 300 {
                let code = (map["code"] as? String) ?? ""
                let isSuccess = (map["success"] as? Bool) ?? false
                let msgKey = map["msgKey"] as? String
                if !isSuccess {
                    let error = DuomMoyaError(code: code, message: msgKey)
                    return .failure(.underlying(error, response))
                } else {
                    var data: Data!
                    if let dataObj = map["obj"] {
                        if dataObj is [String: Any] || dataObj is [Any],
                           let jsonData = try? JSONSerialization.data(withJSONObject: dataObj, options: []) {
                            data = jsonData
                        } else if let str = dataObj as? String {
                            data = str.data(using: .utf8)
                        } else if let intObj = dataObj as? Int {
                            data = intObj.description.data(using: .utf8)
                        } else if let boolObj = dataObj as? Bool {
                            data = boolObj.description.data(using: .utf8)
                        }
                    }
                    
                    if data == nil {
                        data = try? JSONSerialization.data(withJSONObject: [:], options: [])
                    }
                    
                    let processedResponse = Response(statusCode: Int(code)!, data: data, request: response.request, response: response.response)
                    return .success(processedResponse)
                }
            } else {
                return .failure(.underlying(DuomMoyaError(code: "\(response.statusCode)", message: nil), response))
            }
        case let .failure(error):
            return .failure(.underlying(DuomMoyaError(code: "\(error.errorCode)", message: nil), nil))
        }
    }
    
    // 处理Response.data
    private func getDicFromResponseData(_ data: Data) -> [String: Any]? {
        if let result = (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)) as? [String: Any] {
            return result
        }
        return nil
    }
}
