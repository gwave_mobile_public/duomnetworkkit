#
# Be sure to run `pod lib lint DuomNetworkKit.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'DuomNetworkKit'
  s.version          = '0.0.7'
  s.summary          = 'Duom网络请求'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://gitlab.com/gwave_mobile_public/duomnetworkkit'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'HeKai_Duom' => 'kai.he@duom.com' }
  s.source           = { :git => 'https://gitlab.com/gwave_mobile_public/duomnetworkkit.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '12.0'
  s.swift_version    = '5.0'

  
  s.subspec 'MoyaNetwork' do |moya|
      moya.source_files = 'DuomNetworkKit/Classes/MoyaNetwork/**/*'
      moya.dependency 'DuomNetworkKit/Tool'
  end
  
  s.subspec 'HandyJSON' do |json|
      json.source_files = 'DuomNetworkKit/Classes/HandyJSON/**/*'
  end
  
  s.subspec 'Tool' do |tool|
      tool.source_files = 'DuomNetworkKit/Classes/Tool/**/*'
  end
  
  s.subspec 'DuomHUD' do |tool|
      tool.source_files = 'DuomNetworkKit/Classes/DuomHUD/**/*'
  end
  
  s.dependency 'Moya/RxSwift'
  s.dependency 'RxSwift'
  s.dependency 'RxCocoa'
  s.dependency 'HandyJSON'
  s.dependency 'Toast-Swift'
  s.dependency 'MBProgressHUD'
  s.dependency 'DuomBase'
  s.dependency 'lottie-ios'
  
end
