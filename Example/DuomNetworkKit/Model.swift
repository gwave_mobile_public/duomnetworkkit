//
//  ViewModel.swift
//  DuomNetworkKit_Example
//
//  Created by kuroky on 2022/9/1.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import Foundation
import HandyJSON

struct testModel: HandyJSON {
    var id: Int?
    var title: String?
    var imageURL: String?
    
    mutating func mapping(mapper: HelpingMapper) {
        mapper <<< imageURL <-- "image"
    }
}

