import DuomNetworkKit
////
////  API.swift
////  DuomNetworkKit_Example
////
////  Created by kuroky on 2022/9/1.
////  Copyright © 2022 CocoaPods. All rights reserved.
////
//
//import DuomNetworkKit
//
//enum TestAPI {
//    case test(Int)
//}
//
//extension TestAPI: NetworkAPI {
//    var stubBehavior: APIStubBehavior {
//        .never
//    }
//
//    var host: APIHost {
//        return NetworkConfig.baseURL
//    }
//
//    var path: APIPath {
//       return "/post"
//    }
//
//    var parameters: APIParameters? {
//        return ["test": "x12345"]
//    }
//
//    var method: APIMethod {
//        return .get
//    }
//
//    var plugins: APIPlugins {
//        let loading = NetworkLoadingPlugin(autoHide: false)
//        return [loading]
//    }
//
//    var sampleData: Data {
//        switch self {
//        case .test(let count):
//            let number = Int(arc4random_uniform(UInt32(count)))
//            let local = LocalData.init()
//            return local[number]
//        }
//
//    }
//
//
//}
//
//
//struct LocalData {
//
//    subscript(number: Int) -> Data {
//        var data: [String : Any] = [
//            "id": 7,
//            "title": "Network Framework",
//            "image": "https://upload-images.jianshu.io/upload_images/1933747-4bc58b5a94713f99.jpeg"
//        ]
//        var array: [[String : Any]] = []
//        for idx in 0...number {
//            data["id"] = "\(idx)"
//            array.append(data)
//        }
//        let dict: [String : Any] = [
//            "data": array,
//            "code": 200,
//            "message": "successed."
//        ]
//        return D.toJSON(form: dict)!.data(using: String.Encoding.utf8)!
//    }
//}
//
//
//
//
//
//enum BatchAPI {
//    case test
//    case test2(String)
//    case test3
//}
//
//extension BatchAPI: NetworkAPI {
//    
//    var ip: APIHost {
//        return NetworkConfig.baseURL
//    }
//    
//    var path: APIPath {
//        switch self {
//        case .test:
//            return "/ip"
//        case .test2:
//            return "/uuid"
//        case .test3:
//            return "/user-agent"
//        }
//    }
//    
//    var method: APIMethod {
//        return APIMethod.get
//    }
//    
//    var plugins: APIPlugins {
//        let loading = NetworkLoadingPlugin(autoHide: false)
//        return [loading]
//    }
//    
//    var stubBehavior: APIStubBehavior {
//        switch self {
//        case .test3:
//            return APIStubBehavior.delayed(seconds: 2)
//        default:
//            return APIStubBehavior.never
//        }
//    }
//    
//    var sampleData: Data {
//        let dict: [String : Any] = [
//            "data": "delayed 2 seconds return data.",
//            "code": 200,
//            "message": "successed."
//        ]
//        return D.toJSON(form: dict)!.data(using: String.Encoding.utf8)!
//    }
//}

import Moya


enum TTApi {
    case test
}

extension TTApi: NetworkAPI {
    var parameters: [String : Any] {
        return [:]
    }

    var path: String {
        return ""
    }


}
